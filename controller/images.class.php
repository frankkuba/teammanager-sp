<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 15.12.2018
 * Time: 16:08
 */

include_once "database.class.php";

/**
 * Class images - zajištuje práci s obrázky
 */
class images
{

    private $DB;
    const IMAGES_USERS = "images/users/";

    public function __construct()
    {
        $this->DB = new database();
    }

    /** Nahrání obrázku pro konkrétního uživatele a jeho uložení do složky images
     * @param $user login uživatele
     * @return array alert s výsledkem
     */
    public function uploadFile($user)
    {


        $fileDir = self::IMAGES_USERS;
        $fileName = $_FILES['image']['name'];
        $imageSize = $_FILES['image']['size'];
        $fileTmpName = $_FILES['image']['tmp_name'];
        $arr = explode(".", $fileName);
        $fileExt = end($arr);

        $allowed = array("jpg", "jpeg", "png", "gif");  //povolené formáty


        if (in_array(strtolower($fileExt), $allowed) == false) {                //ověření formátu
            return getAlert("Soubor musí být obrázek!", "danger");
        }

        if ($imageSize > 2097152) {                         //ověření velikosti obrázku
             return getAlert("Maximální velikost obrázku je 2 MB", "warning");
         }


        $this->deleteOldImage($user);           //smazání starého
        move_uploaded_file($fileTmpName, $fileDir . $fileName);
        $newFileName = $fileDir . $user . "." . $fileExt;
        rename($fileDir . $fileName, $newFileName);
        $this->resizeImage($newFileName);    //změna velikosti na poměr 1:1
        if ($this->DB->setImage($user, $newFileName)) {

            return getAlert("Obrázek změněn", "info");
        } else {
            return getAlert("Něco se pokazilo", "danger");
        }


    }

    private function deleteOldImage($user)
    {
        $imgName = $this->DB->getImage($user);

        if (isset($imgName) && file_exists($imgName))
            unlink($imgName);
    }

    /**Změna velikosti a ořez na poměr 1:1
     * @param $source_file původní obrázek
     */
    private function resizeImage($source_file){
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];


        switch($mime){
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";

                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";

                break;
        }
        if($width>$height){             //určení nové velikosti
            $new_width = $height;
            $new_height = $height;
        } else {
            $new_width = $width;
            $new_height = $width;
        }
        $dst_img = imagecreatetruecolor($new_width, $new_height);
        $white = imagecolorallocate($dst_img, 255, 255, 255);
        imagefill($dst_img, 0, 0, $white);              //nový obrázek s bílým pozadím
        $src_img = $image_create($source_file);

        $src_x = ($width - $new_width) / 2;
        $src_y = ($height - $new_height) / 2;

        imagecopy($dst_img, $src_img, 0,0, $src_x ,$src_y, $new_width, $new_height);    //kopírování a oříznutí
        unlink($source_file);
        $image($dst_img, $source_file);

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
    }

}