<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 09.12.2018
 * Time: 22:32
 */

include_once "IController.php";

/**
 * Class members - zobrazuje profily uživatelů
 */
class members implements IController
{

    private $DB;

    public function __construct()
    {
        $this->DB = new database();
    }

    public function getResult($data)
    {

        if(isset($_GET["show"]) && ctype_digit($_GET["show"])){         //zobrazení detailu
            $member = $this->DB->getMember($_GET["show"]);

            if(!$member){
                $data["alert"] = getAlert("Uživatel s tímto ID neexistuje", "danger");
            } else {
                $data["member"] = $member;
                return $data;
            }
        }


        $members = $this->DB->getAllActiveUsersInfo();

        $data["members"] = $members;
        return $data;
    }

    public function getPageName()
    {
        // TODO: Implement getPageName() method.
    }

}