<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 09.12.2018
 * Time: 17:15
 */
include_once "controller/IController.php";
include_once "database.class.php";

/**
 * Class events - zobrazení všech akcí
 */
class events implements IController{

private $DB;
public function __construct()
{
    $this->DB = new database();

}
public function getResult($data)
{

    if(isset($_GET["eventid"]) && !empty($_GET["eventid"])){        //zobrazení konkrétní akce
         $data["currPage"] = "eventDetail";
         $data["event"] = $this->DB->getEvent($_GET["eventid"]);
         return $data;

    }

    $data["eventTypes"] = $this->DB->getEventTypes();
    $data["events"] = $this->DB->getNextEvents(15);

    return $data;
}
public function getPageName()
{
    // TODO: Implement getPageName() method.
}
}