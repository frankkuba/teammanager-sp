<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 14.12.2018
 * Time: 22:29
 */

include_once "controller/IController.php";
include_once "database.class.php";

class userAdmin implements IController
{
    private $DB;

    public function __construct()
    {
        $this->DB = new database();
    }

    public function getResult($data)
    {
        if (!isset($data["user"]) || $data["user"]["weight"] < 7) {                                 //ověření, zda má uživatel příslušná práva
            $data["alert"] = getAlert("Na toto nemáte dostatečná oprávnění", "danger");
            $data["currPage"] = "home";                                                             //pokud ne, přesměruje na domovskou stránku
            return $data;
        }
        /** update informací o uživateli */
        if (isset($_POST["action"]) && $_POST["action"] == "updateUser") {
            $data["alert"] = $this->updateUser();
        }

        /** smazání uživatele */
        if (isset($_GET["delete"]) && ctype_digit($_GET["delete"]) && $_GET["delete"] > 1) {
            $result = $this->DB->deleteUser($_GET["delete"]);
            if ($result) {
                $data["alert"] = getAlert("Uživatel úspěšně smazán", "info");
            } else {
                $data["alert"] = getAlert("Tento uživatel neexistuje", "danger");
            }
        }
        /** smazání uživatele pod id, nelze smazat root s id 1 */
        if (isset($_GET["activate"]) && ctype_digit($_GET["activate"]) && $_GET["activate"] > 1) {
            $result = $this->DB->changeActiveUser($_GET["activate"]);
            if ($result) {
                $data["alert"] = getAlert("Status uživatele úspěšně změněn", "info");
            } else {
                $data["alert"] = getAlert("Něco se nepovedlo", "danger");
            }
        }

        /** zobrazení konkrétního uživatele */
        if (isset($_GET["show"]) && ctype_digit($_GET["show"])) {
            $user = $this->DB->getUserForID($_GET["show"]);
            if (isset($user) and !empty($user)) {
                $data["showUser"] = $user;
            } else {
                $data["alert"] = getAlert("Uživatel s tímto ID neexistuje", "danger");
            }
        }


        $data["roles"] = $this->DB->getRights(11);
        $data["users"] = $this->DB->getAllUsersInfo();
        return $data;
    }

    public function getPageName()
    {
        // TODO: Implement getPageName() method.
    }

    /**Ověří uživatele a provede aktualizaci údajů
     * @return array|null
     */
    private function updateUser()
    {
        $alert = null;
        $login = $_POST['login'];
        $name = $_POST["name"];
        $surname = $_POST["surname"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $birthDate = $_POST["birth_day"];



        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return getAlert("Špatně zadaný email", "danger");
        }
        if (!$this->DB->verifyEmail($email)) {
            return getAlert("Tento email je již zaregistrován", "danger");
        }

        if (!$this->DB->updateUser($login, $name, $surname, $email, $phone, $birthDate)) {
            $alert = getAlert("Něco se nepovedlo", "danger");
        }

        if(isset($_POST["role"]) && !empty($_POST["role"])) {
            $role = $_POST["role"];
            $this->DB->updateRole($login, $role);

        }

        if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"])) {
            $imageHandler = new images();
            $alert = $imageHandler->uploadFile($login);
        }
        if (!isset($alert)) {
            $alert = getAlert("Informace o uživateli aktualizovány", "info");
        }
        return $alert;


    }

}