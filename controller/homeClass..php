<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 26.11.2018
 * Time: 21:32
 */


include_once "controller/IController.php";

/**
 * Class home - domovská stránka
 */
class home implements IController
{
private $DB;

    public function __construct()
    {
        $this->DB = new database();

    }

    public function getResult($data){
        $c = 6;
        $data["events"] = $this->DB->getNextEvents($c); //zobrazeni nasledujich 5 udalosti
        return $data;

    }

    public function getPageName()
    {
        // TODO: Implement getPageName() method.
    }
}

?>