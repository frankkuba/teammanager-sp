<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 26.11.2018
 * Time: 21:13
 */

/**
 * Interface IController - základní rozhraní pro controller
 */
interface IController
{
    /** doplní do pole data potřebné informace pro zobrazení
     * @param $data pole dat
     * @return mixed doplněné pole dat
     */
    public function getResult($data);

    public function getPageName();

}