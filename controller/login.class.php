<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 26.11.2018
 * Time: 21:17
 */


include_once "controller/IController.php";


/**
 * Class login - přihlášení uživatele
 */
class login implements IController
{
private $DB;
    function __construct()
    {
        $this->DB = new database();
        define("CURR_PAGE", "login");
    }

    public function getResult($data){
        if(isset($_POST["action"]) && !empty($_POST["action"])){            //pokud je nastavený požadavek, zkusí se přihlášení
            if($_POST["action"] == "login"){
                $data["alert"] = $this->doLogin();
            }

        }

        return $data;

    }

    public function getPageName(){

    }

    private function doLogin(){
        if(isset($_POST["login"]) && !empty($_POST["login"])){
            $username = $_POST["login"];
            $user = $this->DB->loginUser($username);            //zjištění informací o uživateli


            if(!isset($user["login"])){                         //pokud neexistuje zadaný login
                return array("message"=> "login ".$username." neexistuje", "type"=>"danger");
            }

            if($user["active"] == false){
                return array("message"=> "Tento účet ještě není aktivní", "type"=>"warning");
            }
            if(password_verify($_POST["password"], $user["password"])){     //hesla se neshodují
                $_SESSION["user"] = array("login"=>$username, "weight"=>$user["weight"]);   //nastavení SESSION
                session_regenerate_id();
                header("Location: index.php?show=login");           //přesměrování na home
                return;
            } else {
                return array("message"=> "Špatné heslo", "type"=>"danger");

            }
        }
    }
}