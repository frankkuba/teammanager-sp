<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 11.12.2018
 * Time: 15:40
 */
include_once "controller/IController.php";
include_once "database.class.php";

/**
 * Class eventAdmin  - správa akcí
  */
class eventAdmin implements IController
{
    private $DB;
    public function __construct()
    {
        $this->DB = new database();
    }

    public function getResult($data)


    {

        if (!isset($data["user"]) || $data["user"]["weight"] < 7) {         //kontrola oprávnění uživatele
            $data["alert"] = getAlert("Na toto nemáte dostatečná oprávnění", "danger");
            $data["currPage"] = "home";
            return $data;
        }

        if(isset($_POST["action"]) && !empty($_POST["action"])){            //update akce
            $data["alert"] = $this->updateEvent();
        }

        if(isset($_GET["show"]) && ctype_digit($_GET["show"])){             //zobrazení konktérní akce a její úprava
            $event = $this->DB->getEvent($_GET["show"]);
            if(!$event){
                $data["alert"] = getAlert("Tato akce neexistuje", "danger");
                return $data;
            }
            $data["eventType"] = $this->DB->getEventTypes();
            $data["times"] = $this->getTimes();
            $data["showEvent"] = $event;
            return $data;
        }

        if(isset($_GET["delete"]) && ctype_digit($_GET["delete"])){         //smazání akce

            $result = $this->DB->deleteEvent($_GET["delete"]);


            if($result){
                $data["alert"] = getAlert("Událost úspěšně smazána", "info");
            } else{
                $data["alert"] = getAlert("Tato událost neexistuje", "danger");
            }
        }



        $events = $this->DB->getAllEvents();

        $data["events"] = $events;

       return $data;
    }

    public function getPageName()
    {
        // TODO: Implement getPageName() method.
    }

    private function getTimes(){
        $times = array();
        for ($i = 0; $i<24; $i++){
            array_push($times, $i.":00");
        }
        return $times;
    }

    /** Aktualizuje akci
     * @return array alert s výsledkem
     */
    private function updateEvent(){
        $name = $_POST["eventName"];
        $description = $_POST["eventDescription"];
        $dateStart = $_POST["dateStart"];
        $timeStart = $_POST["timeStart"];
        $dateEnd = $_POST["dateEnd"];
        $timeEnd = $_POST["timeEnd"];
        $actionTypeID = $_POST["eventType"];
        $id = $_POST["eventId"];


        $dateTimeStart = $dateStart." ".$timeStart;
        $dateTimeEnd = $dateEnd." ".$timeEnd;

        $result = $this->DB->updateEvent($id, $name, $description, $dateTimeStart, $dateTimeEnd, $actionTypeID);
        if($result){
            return getAlert("Událost úspěšně aktualizována", "info");
        } else{
            return getAlert("Aktualizace události se nepovedla", "danger");
        }
    }

}