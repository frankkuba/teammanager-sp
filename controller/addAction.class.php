<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 05.12.2018
 * Time: 22:55
 */
include_once "controller/IController.php";
include_once "database.class.php";

class addAction implements IController
{

    private $DB, $CURR_PAGE;

    public function __construct()
    {
        $this->DB = new database();
        define("CURR_PAGE", "addAction");
    }

    public function getResult($data)
    {
        if (!isset($data["user"]) || $data["user"]["weight"] < 7) {                               //ověření, zda má uživatel dostatečná oprávnění
            $data["alert"] = getAlert("Na toto nemáte dostatečná prvávnění", "danger");
            $data["currPage"] = "home";
            return $data;
        }

        if(isset($_POST["action"])){
            if($_POST["action"] == "event"){                    //přidání akce
                $data["alert"] = $this->addNewEvent();
            }
        }

        $times = array();
        for ($i = 0; $i<24; $i++){                          //generování časů
            array_push($times, $i.":00");
        }

        $data["eventTypes"] = $this->DB->getEventTypes();
        $data["times"] = $times;
        return $data;

    }

    public function getPageName()
    {
        // TODO: Implement getPageName() method.
    }

    /**Přidá událost do databáze
     * @return array
     */
    private  function addNewEvent(){
        $name = $_POST["eventName"];
        $description = $_POST["eventDescription"];
        $dateStart = $_POST["dateStart"];
        $timeStart = $_POST["timeStart"];
        $dateEnd = $_POST["dateEnd"];
        $timeEnd = $_POST["timeEnd"];
        $actionTypeID = $_POST["eventType"];
        $login = $_SESSION["user"]["login"];

        $dateTimeStart = $dateStart." ".$timeStart;
        $dateTimeEnd = $dateEnd." ".$timeEnd;

        if($this->DB->addEvent($name,$description,$dateTimeStart, $dateTimeEnd, $actionTypeID, $login)){
            return getAlert("Událost úspěšně přidána", "info");
        } else{
            return getAlert("Něco se nepovedlo", "danger");
        }


    }
}