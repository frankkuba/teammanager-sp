<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 02.12.2018
 * Time: 17:30
 */
include_once "controller/IController.php";
include_once "database.class.php";

class registration implements IController{

    private $DB;
    private $CURR_NAME;
    public function __construct()
    {
        $this->DB = new database();
        define("CURR_NAME", "registrace");
    }

    public function getResult($data){
        $rights = $this->DB->getRights(7);          //získání rolí s maximální váhou 6

        $data["rights"] = $rights;

        if(isset($_POST["action"]) && !empty($_POST["action"])){
            if($_POST["action"] == "register"){                             //požadavek na registraci
                $data["alert"] = $this->register();

            }

        }

        return $data;
    }

    public function getPageName(){

    }

    private function register(){
        /* ověření ReCaptcha */
        $recaptcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=6LfGEYAUAAAAACViAKz4da7SZ8zmm_A23t7MzUXM&response='
            . $_POST['g-recaptcha-response']));
        if ($recaptcha->{'success'} != 'true') {
           return getAlert("Neověřený uživatel, zkuste to prosím znovu", "danger");
        }

        $login = $_POST["login"];
        $passHash = password_hash($_POST["password"], PASSWORD_BCRYPT);     //hash hesla
        $name = $_POST["name"];
        $surname = $_POST["surname"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $roleID = $_POST["role"];                                               //ověřování podímínek pro registraci
        if($this->DB->verifyLoginExits($login)){
            return getAlert("Uživatel s tímto loginem již existuje", "danger");
        }
        if(!$this->DB->verifyEmail($email)){
            return getAlert("Tento email je již zaregistrován", "danger");
        }
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            return getAlert("Špatně zadaný email", "danger");
        }
        if($this->DB->registerUser($login,$passHash,$name,$surname,$email,$phone,$roleID)){
            header("Location: index.php?show=registered");
        } else {
            return getAlert("Něco se pokazilo, kontaktujte administrátora", "danger");
        }

    }



}