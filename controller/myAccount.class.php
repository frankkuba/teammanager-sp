<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 03.12.2018
 * Time: 20:23
 */
include_once "controller/IController.php";
include_once "database.class.php";
include_once "controller/images.class.php";

class myAccount implements IController
{

    private $DB;
    const CURR_PAGE_NAME = "Můj účet";



    public function __construct()
    {

        $this->DB = new database();
    }

    public function getResult($data)
    {
        if (isset($_GET["alert"])) {                  //pokud byl zmenen obrazek, zobrazi se alert
            if ($_GET["alert"] == "imageChanged") {
                $data["alert"] = getAlert("Obrázek změněn", "info");
            }
        }

        if (isset($_POST["action"]) && $_POST["action"] == "updateUser") {
            $alert = $this->updateUserInfo();
        }
        if (isset($alert) && $alert != null) {
            $data["alert"] = $alert;                        //nastavi alert
        }
        $userInfo = $this->getUserInfo($_SESSION["user"]["login"]); //nastavi info o uzivateli
        $filemtime = filemtime($userInfo["image_name"]);
        $newImageName = $userInfo["image_name"] . "?" . $filemtime;
        $userInfo["image_name"] = $newImageName;

        $data["user"] = $userInfo;


        $date1 = date_create("2013");
        $date2 = date_create("2020");
        $diff = date_diff($date1, $date2);



        return $data;
    }

    public function getPageName()
    {

    }


    private function getUserInfo($username)
    {
        if (!empty($username)) {
            $info = $this->DB->getCompleteUserInfo($username);
            if (isset($info["login"]) && !empty($info["login"])) {
                return $info;
            }
        }
        return null;

    }


    private function updateUserInfo()
    {
        $alert = null;
        $login = $_SESSION["user"]['login'];
        $name = $_POST["name"];
        $surname = $_POST["surname"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $birthDate = $_POST["birth_day"];
        //ověření všech podmínek pro aktualizaci dat

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return getAlert("Špatně zadaný email", "danger");
        }
        if (!$this->DB->verifyEmail($email)) {
            return getAlert("Tento email je již zaregistrován", "danger");
        }

        if (!$this->DB->updateUser($login, $name, $surname, $email, $phone, $birthDate)) {
            $alert = getAlert("Něco se nepovedlo", "danger");
        }
        if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"])) {
            $imageHandler = new images();
            $alert = $imageHandler->uploadFile($login);
        }
        if (!isset($alert)) {
            $alert = getAlert("Informace o uživateli aktualizovány", "info");
        }
        return $alert;


    }




}