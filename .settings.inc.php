<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 24.11.2018
 * Time: 22:48
 */


global $PAGE_NAME;
global $FAVICO;
global $CONTROLLER_KEY;
global $LOGO;
global $CSS_FILE;
global $db_server, $db_name, $db_pass, $db_user;

define("CONTROLLER_KEY", "cont");


// database
$db_server = "localhost";
$db_name = "team_manager";
$db_user = "root";
$db_pass = "";


define("PAGE_NAME", "Team Manager");
define("PAGES", [
    "home" => ["name" => "Home", CONTROLLER_KEY => "home"],
    "myAccount" => ["name" => "Můj účet", CONTROLLER_KEY => "myAccount"],
    "registration" => ["name" => "Registrace", CONTROLLER_KEY => "registration"],
    "login" => ["name" => "Přihlášení", CONTROLLER_KEY => "login"],
    "addAction" => ["name" => "Nová akce", CONTROLLER_KEY => "addAction"],
    "events" => ["name"=> "Akce", CONTROLLER_KEY => "events"],
    "members" => ["name"=> "Lidé", CONTROLLER_KEY => "members"],
    "eventAdmin" => ["name"=> "Správa akcí", CONTROLLER_KEY => "eventAdmin"],
    "userAdmin" => ["name"=> "Správa uživatelů", CONTROLLER_KEY => "userAdmin"]
]);

define("CSS_FILE", "css/default.css");
define("FAVICO", "/imgages/favico.ico");
define("LOGO", "/images/logo.jpg");
define("DEFAULT_PAGE", "home");

