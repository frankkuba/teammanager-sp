<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 02.12.2018
 * Time: 17:13
 */

/**
 * Class database - zajišťuje komunikaci s databází
 */
class database
{
    private $pdo;

    /**
     * database constructor.
     */
    public function __construct()
    {
        global $db_server, $db_name, $db_pass, $db_user;
        $this->pdo = new PDO("mysql:host=$db_server;dbname=$db_name", $db_user, $db_pass);
        $q = "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'";
        $this->pdo->query($q);
    }


    public function getRights($value = 10)
    {
        $stm = $this->pdo->prepare("SELECT * FROM roles where weight< :value ");
        $stm->bindValue(":value", $value, PDO::PARAM_INT);
        $stm->execute();
        $res = $stm->fetchAll();
        return $res;

    }

    public function getEventTypes()
    {
        $stm = $this->pdo->prepare("SELECT * FROM event_type");
        $stm->execute();
        return $stm->fetchAll();

    }


    public function loginUser($username)
    {
        $stm = $this->pdo->prepare("SELECT user.active, user.login, user.password, roles.weight FROM user INNER JOIN roles ON user.ROLES_role_id=roles.role_id
                                              where user.login = :login");
        $stm->bindValue(":login", $username);
        $stm->execute();
        return $stm->fetch();
    }

    public function verifyLoginExits($username)
    {
        $stm = $this->pdo->prepare("SELECT COUNT(login) from user where login = :username");
        $stm->bindValue(":username", $username);
        $stm->execute();
        $result = $stm->fetchColumn(0);
        if (intval($result) > 0)
            return true;
        else
            return false;

    }

    public function verifyEmail($email)
    {
        $stm = $this->pdo->prepare("SELECT COUNT(login) from user where email = :email");
        $stm->bindValue(":email", $email);
        $stm->execute();
        $result = $stm->fetchColumn(1);
        if (intval($result) > 0)
            return false;
        else
            return true;

    }

    public function getCompleteUserInfo($username)
    {
        $stm = $this->pdo->prepare("SELECT user.login, user.name, user.surname, user.email, user.phone, user.birth_day, user.image_name, user.ROLES_role_id, roles.role_name, roles.weight 
                                               FROM user INNER JOIN roles ON user.ROLES_role_id=roles.role_id where login = :login");
        $stm->bindValue(":login", $username);
        $stm->execute();
        return $stm->fetch();
    }

    public function getUserForID($id)
    {
        $stm = $this->pdo->prepare("SELECT user.login, user.id, user.name, user.surname, user.email, user.phone, user.birth_day, user.image_name, roles.role_id, roles.role_name, roles.weight
                                               FROM user INNER JOIN roles ON user.ROLES_role_id=roles.role_id where id = :id");
        $stm->bindValue(":id", $id);
        $stm->execute();
        return $stm->fetch();
    }

    public function changeActiveUser($id){
        $stm = $this->pdo->prepare("UPDATE user set active = NOT active where user.id = :id");
        $stm->bindValue(":id", $id, PDO::PARAM_INT);
        $res = $stm->execute();
        return $res;
    }


    public function registerUser($login, $passHash, $name, $surname, $email, $phone, $roleID)
    {
        $stm = $this->pdo->prepare("INSERT INTO user(login, password, name, surname, email, phone, ROLES_role_id, image_name) 
                                          values (:login, :hash, :name, :surname, :email, :phone, :role_id, \"images/user_default.jpg\")");
        $stm->bindValue(":login", $login);
        $stm->bindValue(":hash", $passHash);
        $stm->bindValue("name", $name);
        $stm->bindValue(":surname", $surname);
        $stm->bindValue(":email", $email);
        $stm->bindValue(":phone", $phone);
        $stm->bindValue(":role_id", $roleID);
        if ($stm->execute())
            return true;
        else
            var_dump($stm->errorInfo());
        return false;
    }

    public function updateUser($login, $name, $surname, $email, $phone, $birthDate)
    {
        $stm = $this->pdo->prepare("UPDATE user set name = :name, surname = :surname, email = :email, 
                                              phone = :phone, birth_day = :birthDate where login = :login");
        $stm->bindValue(":name", $name);
        $stm->bindValue(":surname", $surname);
        $stm->bindValue(":email", $email);
        $stm->bindValue(":phone", $phone);
        $stm->bindValue(":birthDate", $birthDate);
        $stm->bindValue(":login", $login);
        return $stm->execute();

    }

    public function updateRole($login, $roleID){
        $stm = $this->pdo->prepare("UPDATE user set ROLES_role_id = :roleID where user.login = :login");
        $stm->bindValue("roleID", $roleID, PDO::PARAM_INT);
        $stm->bindValue(":login", $login);
        $stm->execute();
    }

    public function getImage($username)
    {
        $stm = $this->pdo->prepare("SELECT image_name from USER where login = :login");
        $stm->bindValue(":login", $username);
        $stm->execute();
        return $stm->fetchColumn(0);
    }

    public function setImage($username, $image)
    {
        $stm = $this->pdo->prepare("UPDATE user SET image_name = :img WHERE login = :username");
        $stm->bindValue(":img", $image);
        $stm->bindValue(":username", $username);
        return $stm->execute();
    }


    public function addEvent($name, $description, $dateStart, $dateEnd, $eventTypeID, $login)
    {
        $stm = $this->pdo->prepare("INSERT INTO event(name, description,time_start, time_end, EVENT_TYPE_id, AUTHOR_id) VALUES 
        (:name, :description, :dateStart, :dateEnd, :eventTypeID, (SELECT user.id FROM user WHERE user.login = :login))");
        $stm->bindValue(":name", $name);
        $stm->bindValue(":description", $description);
        $stm->bindValue(":dateStart", $dateStart);
        $stm->bindValue(":dateEnd", $dateEnd);
        $stm->bindValue(":eventTypeID", $eventTypeID);
        $stm->bindValue(":login", $login);
        return $stm->execute();
    }


    public function getAllEvents()
    {
        $stm = $this->pdo->prepare("SELECT event.id,event.name, event.description, event.time_start, event.time_end,  CONCAT(user.name, \" \", user.surname) AS author, event.AUTHOR_id, event_type.name AS event_type 
FROM event INNER JOIN user on user.id = event.AUTHOR_id INNER JOIN event_type on event_type.id = event.EVENT_TYPE_id order by event.time_start");
        $stm->execute();
        return $stm->fetchAll();
    }

    public function getNextEvents($count = 1000)
    {
        $stm = $this->pdo->prepare("SELECT event.id, event.name, event.description, event.time_start, event.time_end,  CONCAT(user.name, \" \", user.surname) AS author, event.AUTHOR_id, event_type.name AS event_type 
FROM event INNER JOIN user on user.id = event.AUTHOR_id INNER JOIN event_type on event_type.id = event.EVENT_TYPE_id 
where event.time_end > CURRENT_DATE order by event.time_start limit :index");

        $stm->bindValue(":index", $count, PDO::PARAM_INT);
        $stm->execute();
        $arr = $stm->fetchAll();

        return $arr;
    }

    public function getPreviousEvents($typeID, $count = 1000)
    {
        $stm = $this->pdo->prepare("SELECT event.id, event.name, event.description, event.time_start, event.time_end,  CONCAT(user.name, \" \", user.surname) AS author, event.AUTHOR_id, event_type.name AS event_type 
FROM event INNER JOIN user on user.id = event.AUTHOR_id INNER JOIN event_type on event_type.id = event.EVENT_TYPE_id 
where event.time_start < CURRENT_DATE and event.EVENT_TYPE_id = :typeID order by event.time_start DESC limit :index");
        $stm->bindValue(":typeID", $typeID, PDO::PARAM_INT);
        $stm->bindValue(":index", $count, PDO::PARAM_INT);
        $stm->execute();
        $arr = $stm->fetchAll();

        return $arr;
    }

    public function getEvent($id)
    {
        $stm = $this->pdo->prepare("SELECT event.id, event.name, event.description, event.time_start, event.time_end, user.image_name, CONCAT(user.name, \" \", user.surname) AS author, event.AUTHOR_id, event_type.name AS event_type,
 event_type.id AS event_typeID
FROM event INNER JOIN user on user.id = event.AUTHOR_id INNER JOIN event_type on event_type.id = event.EVENT_TYPE_id where event.id = :id");
        $stm->bindValue(":id", $id, PDO::PARAM_INT);
        $stm->execute();
        return $stm->fetch();

    }

    public function getUsersForRole($roleID)
    {
        $stm = $this->pdo->prepare("SELECT CONCAT(user.name, \" \",user.surname) as full_name, user.image_name from user where user.ROLES_role_id = :roleID and user.active = true order by full_name");
        $stm->bindValue(":roleID", $roleID);
        $stm->execute();
        return $stm->fetchAll();
    }

    public function getAllUsersInfo()
    {
        $stm = $this->pdo->prepare("SELECT user.active, CONCAT(user.name, \" \",user.surname) as full_name, user.id, user.image_name, roles.role_name from user inner join roles on user.ROLES_role_id =roles.role_id  order by user.ROLES_role_id, full_name");

        $stm->execute();
        return $stm->fetchAll();
    }
    public function getAllActiveUsersInfo()
    {
        $stm = $this->pdo->prepare("SELECT CONCAT(user.name, \" \",user.surname) as full_name, user.id, user.image_name, roles.role_name from user inner join roles on user.ROLES_role_id =roles.role_id
                                            where user.active = true order by user.ROLES_role_id, full_name");

        $stm->execute();
        return $stm->fetchAll();
    }

    public function getMember($id)
    {
        $stm = $this->pdo->prepare("SELECT user.name, user.surname, user.email, user.phone, user.image_name, user.birth_day, roles.role_name, roles.weight
        from user inner  join roles on user.ROLES_role_id = roles.role_id where user.id = :id and user.active = true");
        $stm->bindValue(":id", $id, PDO::PARAM_INT);
        $stm->execute();
        return $stm->fetch();

    }

    public function deleteEvent($id)
    {
        $stm = $this->pdo->prepare("DELETE FROM event where event.id = :id");
        $stm->bindValue(":id", $id);
        return $stm->execute();

    }

    public function deleteUser($id)
    {
        $stm = $this->pdo->prepare("DELETE FROM user where user.id = :id");
        $stm->bindValue(":id", $id, PDO::PARAM_INT);
        return $stm->execute();
    }

    public function updateEvent($id, $name, $description, $dateStart, $dateEnd, $eventTypeID)
    {
        $stm = $this->pdo->prepare("UPDATE event set name = :name, description = :description, time_start = :time_start, time_end = :time_end,
                                              EVENT_TYPE_id = :event_type_id WHERE id = :id");
        $stm->bindValue(":name", $name);
        $stm->bindValue(":description", $description);
        $stm->bindValue(":time_start", $dateStart);
        $stm->bindValue(":time_end", $dateEnd);
        $stm->bindValue(":event_type_id", $eventTypeID);
        $stm->bindValue(":id", $id, PDO::PARAM_INT);
        return $stm->execute();


    }


}