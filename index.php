<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 19.11.2018
 * Time: 21:27
 */


include_once ".settings.inc.php";
include_once "vendor/autoload.php";

include_once "controller/homeClass..php";
include_once "controller/login.class.php";
include_once "controller/registration.class.php";
include_once "controller/myAccount.class.php";
include_once "controller/addAction.class.php";
include_once "controller/events.class.php";
include_once "controller/members.class.php";
include_once "controller/eventAdmin.class.php";
include_once "controller/userAdmin.class.php";

global $currPage;       //aktuální stránka

ini_set('session.cache_limiter', 'public');              //povolení návratu na předchozí stránku
session_cache_limiter(false);
ini_set("session.use_strict_mode", true);
session_start();                                                        //začátek SESSION

if (isset($_REQUEST["page"]) && key_exists($_REQUEST["page"], PAGES)) {

    $currPage = PAGES[$_REQUEST["page"]][CONTROLLER_KEY];       //ověření, zda je stránka v seznamu stránek
} else {
    $currPage = PAGES[DEFAULT_PAGE][CONTROLLER_KEY];            //pokud není
}
$data = ["currPage" => $currPage, "pageName" => PAGE_NAME, "currPageName" => PAGES[$currPage]["name"]];  //inicializace dat

if (isset($_SESSION["user"])) {
    $data["user"] = $_SESSION["user"];
}

$controller = new $data["currPage"];
$data = $controller->getResult($data);          //získání dat z controlleru


$data = setAlert($data);

$loader = new Twig_Loader_Filesystem("templates");

$enviroment = new Twig_Environment($loader);


echo $enviroment->render($data["currPage"] . ".twig", $data); //vyrenderuje stránku z šablony a zobrazí



/**Vytvoří alert a vrátí jako pole
 * @param $message text upozornění
 * @param $type typ upozornění (bootstrap alert)
 * @return array alert jako pole
 */
function getAlert($message, $type)
{
    return array("message" => $message, "type" => $type);
}

/** Nastaví alert na hlavní stránce, pokud je nastavený příslušný GET parametr
 * @param $data pole dat pro stránku
 * @return mixed data + alert
 */
function setAlert($data)
{
    if (isset($_GET["show"])) {
        if ($_GET["show"] == "login") {
            $data["alert"] = getAlert("Přihlášení proběhlo úspěšně.", "info");
        } else if ($_GET["show"] == "logout") {
            $data["alert"] = getAlert("Uživatel byl úspěšně odhlášen.", "info");
        } else if ($_GET["show"] == "registered") {
            $data["alert"] = getAlert("Registrace byla úspěšná, počkejte, než administrátor aktivuje váš účet.", "info");
        }
    }
    return $data;
}





